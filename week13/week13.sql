Create Temporary Table products_below_avg
 Select productid, productname, price 
 From products 
 Where price < (Select Avg(price) From products);

Drop Table products_below_avg;
    
Show Table Status;

Select * From products_below_avg;

Show Tables;